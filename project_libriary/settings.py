import os


DATABASE_URL = os.getenv('DATABASE_URL', 'sqlite:///test.db')
DEBUG = True

SECRET_KEY = 'development key'
