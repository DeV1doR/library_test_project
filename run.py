from flask import Flask
from flask.ext.login import LoginManager


from apps.libriary.views import (
    BookAPI, AuthorAPI, MainPageView, SearchView, BookAuthorAPI,
    CreateRelView, LoginView, LogoutView
)
from apps.libriary.models import User, Anonymous
from database import db_session


app = Flask(__name__)
app.config.from_object('project_libriary.settings')
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.anonymous_user = Anonymous


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@login_manager.user_loader
def load_user(user_id):
    return db_session.query(User).get(user_id)


def register_api(view, endpoint, url, pk='id', pk_type='int'):
    view_func = view.as_view(endpoint)
    app.add_url_rule(url, defaults={pk: None},
                     view_func=view_func, methods=['GET', ])
    app.add_url_rule(url, view_func=view_func, methods=['POST', ])
    app.add_url_rule('%s<%s:%s>' % (url, pk_type, pk), view_func=view_func,
                     methods=['GET', 'PUT', 'DELETE'])


# api
register_api(BookAPI, 'books_api', '/api/v1/books/')
register_api(AuthorAPI, 'authors_api', '/api/v1/authors/')
app.add_url_rule(
    '/api/v1/books/authors/',
    view_func=BookAuthorAPI.as_view('books_authors_api'),
    methods=['POST', 'DELETE'])

# ---
app.add_url_rule(
    '/', view_func=MainPageView.as_view('main_page'))
app.add_url_rule(
    '/search', view_func=SearchView.as_view('search'), methods=['GET', ])
app.add_url_rule(
    '/rel/create/',
    view_func=CreateRelView.as_view('rel_create'), methods=['POST', 'GET'])
app.add_url_rule(
    '/login/',
    view_func=LoginView.as_view('login_page'), methods=['GET', 'POST'])
app.add_url_rule(
    '/logout/',
    view_func=LogoutView.as_view('logout'), methods=['GET', ])

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000)
