# Libriary

## Project link
http://libriary.herokuapp.com/ 


## Installation
1. virtualenv <env name>;
2. source <env name>/bin/activate;
3. pip install -r requirements.txt;
4. bower install (suggested, that you have node and npm);
5. python create_db (to fill db or use manualy sql script in apps/libriary/fixtures/sql/;
5. python run.py (to start app);


To login as admin use:
 -u admin -p admin

To login as simple user:
 -u foo -p foo

On the develop branch, search filter created on sockets.
