from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


from project_libriary import settings


engine = create_engine(settings.DATABASE_URL, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    import apps.libriary.models
    Base.metadata.create_all(bind=engine)


def drop_db():
    import apps.libriary.models
    Base.metadata.drop_all(bind=engine)
