from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash


from flask.ext.login import UserMixin, AnonymousUserMixin


from database import Base


class ExtendedUserMixin(UserMixin):

    def get_role(self):
        return self.role

    def has_role(self, role):
        return bool(self.get_role() == role)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)


class Anonymous(AnonymousUserMixin, ExtendedUserMixin):

    role = 'user'


class User(Base, ExtendedUserMixin):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(25), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    email = Column(String(255), unique=True)
    is_active = Column(Boolean(), nullable=False, default=True)
    is_authenticated = Column(Boolean(), nullable=False, default=True)
    is_anonymous = Column(Boolean(), nullable=False, default=False)
    first_name = Column(String(50), default='')
    last_name = Column(String(50), default='')
    role = Column(String(50), default='user')

    def __init__(self, username=None, password=None):
        self.username = username
        self.set_password(password)

    def __repr__(self):
        return '<User %r>' % (self.username)


class BookAuthor(Base):

    __tablename__ = 'books_authors'

    book_id = Column(Integer, ForeignKey('books.id'), primary_key=True)
    author_id = Column(Integer, ForeignKey('authors.id'), primary_key=True)
    book = relationship(
        "Book", back_populates="authors")
    author = relationship(
        "Author", back_populates="books")


class Book(Base):

    __tablename__ = 'books'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(20), unique=True, nullable=False)
    authors = relationship(
        'BookAuthor', back_populates="book",
        cascade="all")

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Book %r>' % (self.name)

    def __json__(self):
        return ['id', 'name', 'authors']

    def to_json(self):
        book = self.__dict__.copy()
        del book['_sa_instance_state']
        authors = []
        for q in self.authors:
            author = q.author.__dict__.copy()
            del author['_sa_instance_state']
            if author.get('books', None):
                del author['books']
            authors.append(author)
        book['authors'] = authors
        return book


class Author(Base):

    __tablename__ = 'authors'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(20), unique=True, nullable=False)
    books = relationship(
        'BookAuthor', back_populates="author",
        cascade="all")

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Author %r>' % (self.name)

    def __json__(self):
        return ['id', 'name', 'books']

    def to_json(self):
        author = self.__dict__.copy()
        del author['_sa_instance_state']
        books = []
        try:
            for q in self.books:
                book = q.book.__dict__.copy()
                del book['_sa_instance_state']
                if book.get('authors', None):
                    del book['authors']
                books.append(book)
        except AttributeError:
            pass
        author['books'] = books
        return author
