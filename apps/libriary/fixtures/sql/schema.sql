PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE user_roles (
	id INTEGER NOT NULL, 
	user_id INTEGER, 
	role_id INTEGER, 
	PRIMARY KEY (id), 
	FOREIGN KEY(user_id) REFERENCES users (id), 
	FOREIGN KEY(role_id) REFERENCES roles (id)
);
CREATE TABLE roles (
	id INTEGER NOT NULL, 
	name VARCHAR(50), 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
CREATE TABLE users_roles (
	id INTEGER NOT NULL, 
	user_id INTEGER, 
	role_id INTEGER, 
	PRIMARY KEY (id), 
	FOREIGN KEY(user_id) REFERENCES users (id), 
	FOREIGN KEY(role_id) REFERENCES roles (id)
);
CREATE TABLE books (
	id INTEGER NOT NULL, 
	name VARCHAR(20) NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
INSERT INTO "books" VALUES(1,'Book 0');
INSERT INTO "books" VALUES(2,'Book 1');
INSERT INTO "books" VALUES(3,'Book 2');
INSERT INTO "books" VALUES(4,'Book 3');
INSERT INTO "books" VALUES(5,'Book 4');
INSERT INTO "books" VALUES(6,'Book 5');
INSERT INTO "books" VALUES(7,'Book 6');
INSERT INTO "books" VALUES(8,'Book 7');
INSERT INTO "books" VALUES(9,'Book 8');
INSERT INTO "books" VALUES(10,'Book 9');
INSERT INTO "books" VALUES(11,'Book 10');
INSERT INTO "books" VALUES(12,'Book 11');
INSERT INTO "books" VALUES(13,'Book 12');
CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(25) NOT NULL, 
	password VARCHAR(255) NOT NULL, 
	email VARCHAR(255), 
	is_active BOOLEAN NOT NULL, 
	is_authenticated BOOLEAN NOT NULL, 
	is_anonymous BOOLEAN NOT NULL, 
	first_name VARCHAR(50), 
	last_name VARCHAR(50), 
	role VARCHAR(50), 
	PRIMARY KEY (id), 
	UNIQUE (username), 
	UNIQUE (email), 
	CHECK (is_active IN (0, 1)), 
	CHECK (is_authenticated IN (0, 1)), 
	CHECK (is_anonymous IN (0, 1))
);
INSERT INTO "users" VALUES(1,'admin','pbkdf2:sha1:1000$XiHMFeDW$a07fe8658576f1bb5683ea5b988b65105ae1e3cf',NULL,1,1,0,'','','admin');
INSERT INTO "users" VALUES(2,'foo','pbkdf2:sha1:1000$SczAbxfS$a8b26897d54ab85fcd2e8a05302caa67e9358099',NULL,1,1,0,'','','user');
CREATE TABLE authors (
	id INTEGER NOT NULL, 
	name VARCHAR(20) NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
INSERT INTO "authors" VALUES(1,'Author 0');
INSERT INTO "authors" VALUES(2,'Author 1');
INSERT INTO "authors" VALUES(3,'Author 2');
INSERT INTO "authors" VALUES(4,'Author 3');
INSERT INTO "authors" VALUES(5,'Author 4');
INSERT INTO "authors" VALUES(6,'Author 5');
INSERT INTO "authors" VALUES(7,'Author 6');
INSERT INTO "authors" VALUES(8,'Author 7');
INSERT INTO "authors" VALUES(9,'Author 8');
INSERT INTO "authors" VALUES(10,'Author 9');
INSERT INTO "authors" VALUES(11,'Author 10');
INSERT INTO "authors" VALUES(12,'Author 11');
INSERT INTO "authors" VALUES(13,'Author 12');
CREATE TABLE books_authors (
	book_id INTEGER NOT NULL, 
	author_id INTEGER NOT NULL, 
	PRIMARY KEY (book_id, author_id), 
	FOREIGN KEY(book_id) REFERENCES books (id), 
	FOREIGN KEY(author_id) REFERENCES authors (id)
);
INSERT INTO "books_authors" VALUES(1,1);
INSERT INTO "books_authors" VALUES(2,2);
INSERT INTO "books_authors" VALUES(3,3);
INSERT INTO "books_authors" VALUES(4,4);
INSERT INTO "books_authors" VALUES(5,5);
INSERT INTO "books_authors" VALUES(6,6);
INSERT INTO "books_authors" VALUES(7,7);
INSERT INTO "books_authors" VALUES(8,8);
INSERT INTO "books_authors" VALUES(9,9);
INSERT INTO "books_authors" VALUES(10,10);
INSERT INTO "books_authors" VALUES(11,11);
INSERT INTO "books_authors" VALUES(11,12);
INSERT INTO "books_authors" VALUES(12,13);
INSERT INTO "books_authors" VALUES(13,13);
COMMIT;
