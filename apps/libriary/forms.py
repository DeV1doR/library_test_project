from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import ValidationError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound


from database import db_session
from apps.libriary.models import Book, Author, User


class BookAuthorForm(Form):

    book = QuerySelectField(
        'Book', query_factory=db_session.query(Book).all,
        get_pk=lambda a: a.id, get_label=lambda a: a.name)
    author = QuerySelectField(
        'Author', query_factory=db_session.query(Author).all,
        get_pk=lambda a: a.id, get_label=lambda a: a.name)


class LoginForm(Form):

    username = TextField('Username')
    password = PasswordField('Password')

    def validate_username(self, field):
        try:
            db_session.query(User).filter_by(
                username=self.data['username']).one()
        except NoResultFound:
            raise ValidationError(
                'User - <User: %s> does not exist.' % self.data['username'])
        except MultipleResultsFound:
            raise ValidationError(
                'There are many users with this username.')

    def validate_password(self, field):
        user = db_session.query(User).filter_by(
            username=self.data['username']).first()
        if user and not user.check_password(self.data['password']):
            raise ValidationError('Invalid password.')
