# -*- coding: utf-8 -*-
import json
from flask import render_template, request, redirect
from flask.views import MethodView, View
from flask.ext.api import status
from flask.ext.login import login_user, logout_user
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError


from database import db_session
from apps.libriary.models import Book, Author, BookAuthor, User
from apps.libriary.forms import BookAuthorForm, LoginForm
from apps.libriary.utils import (
    BookAuthorEncoder, get_or_create, login_required
)


app_template = 'libriary/'


class LoginView(View):

    template = app_template + 'login_page.html'
    class_form = LoginForm
    model = User
    success_url = '/'

    def get_context(self):
        return dict(form=self.get_class_form())

    def get_class_form(self):
        return self.class_form(request.form, csrf_enabled=False)

    def get_template_name(self):
        return self.template

    def dispatch_request(self):
        if request.method == 'POST':
            return self.post()
        return self.render_template(self.get_context())

    def render_template(self, context):
        return render_template(self.get_template_name(), **context)

    def post(self):
        form = self.get_class_form()
        if form.validate_on_submit():
            username = form.data['username']
            password = form.data['password']
            user = db_session.query(User).filter_by(username=username).one()
            if user.check_password(password):
                login_user(user)
                return redirect(self.success_url)
        return render_template(self.get_template_name(), form=form)


class LogoutView(View):

    success_url = '/'

    def dispatch_request(self):
        logout_user()
        return redirect(self.success_url)


class ListView(View):

    context_objects = 'objects'

    def get_template_name(self):
        raise NotImplementedError()

    def render_template(self, context):
        return render_template(self.get_template_name(), **context)

    def dispatch_request(self):
        return self.render_template(self.get_context())

    def get_context(self):
        context = {self.context_objects: self.get_objects()}
        return context


class LibriaryAPI(MethodView):

    decorators = [login_required(role="admin"), ]

    def dispatch_request(self, *args, **kwargs):
        if request.method in ['POST', 'PUT']:
            name = request.values.get('name', None)
            if not name:
                return json.dumps({
                    "status": status.HTTP_400_BAD_REQUEST,
                    "reason": "Name field is empty"
                }), status.HTTP_400_BAD_REQUEST
        return super(LibriaryAPI, self).dispatch_request(*args, **kwargs)

    def get(self, id):
        if id is None:
            q = db_session.query(self.model).all()
        else:
            q = db_session.query(self.model).get(id)
        return json.dumps(q, cls=BookAuthorEncoder), status.HTTP_200_OK

    def post(self):
        name = request.values['name']
        try:
            q = self.model(name=name)
            db_session.add(q)
            db_session.commit()
        except IntegrityError:
            return json.dumps({
                "status": status.HTTP_400_BAD_REQUEST,
                "reason": "%s name - %s already exists." % (
                    q.__class__.__name__, name)
            }), status.HTTP_400_BAD_REQUEST
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "%s - %s successfully created." % (
                q.__class__.__name__, q.name),
            "data": q.to_json()
        }), status.HTTP_200_OK

    def put(self, id):
        name = request.values['name']
        try:
            q = db_session.query(self.model).get(id)
            q.name = name.strip('\t\n\r')
            db_session.commit()
        except AttributeError:
            return json.dumps({
                "status": status.HTTP_404_NOT_FOUND,
                "reason": "Query doest not exist."
            }), status.HTTP_404_NOT_FOUND
        except IntegrityError:
            return json.dumps({
                "status": status.HTTP_400_BAD_REQUEST,
                "reason": "%s name - %s already exists." % (
                    q.__class__.__name__, name)
            }), status.HTTP_400_BAD_REQUEST
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "%s - %s successfully updated." % (
                q.__class__.__name__, q.name)
        }), status.HTTP_200_OK

    def delete(self, id):
        q = db_session.query(self.model).get(id)
        try:
            db_session.delete(q)
            db_session.commit()
            return json.dumps({
                "status": status.HTTP_200_OK,
                "reason": "%s - %s successfully deleted." % (
                    q.__class__.__name__, q.name)
            }), status.HTTP_200_OK
        except (UnmappedInstanceError, AttributeError):
            return json.dumps({
                "status": status.HTTP_404_NOT_FOUND,
                "reason": "Query doest not exist."
            }), status.HTTP_404_NOT_FOUND


class BookAPI(LibriaryAPI):

    model = Book
    rel_model = BookAuthor


class AuthorAPI(LibriaryAPI):

    model = Author
    rel_model = BookAuthor


class MainPageView(ListView):

    template_name = app_template + 'objects_list.html'
    model = Book
    form = BookAuthorForm

    def get_template_name(self):
        return self.template_name

    def get_form(self):
        return self.form(request.form, csrf_enabled=False)

    def get_objects(self):
        return db_session.query(self.model).order_by('-id').all()

    def get_context(self):
        context = super(MainPageView, self).get_context()
        context['form'] = self.get_form()
        return context

    def dispatch_request(self):
        form_name = request.values.get('form', None)
        if form_name and form_name == 'complex':
            return self.get_complex_form_info()
        if form_name:
            return self.get_html_form(form_name)
        return super(MainPageView, self).dispatch_request()

    def get_html_form(self, form_name):
        from jinja2 import Template
        template = Template('{{ form.%s }}' % form_name)
        return template.render(form=self.get_form())

    def get_complex_form_info(self):
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "Forms successfully updated.",
            "data": {
                "select_book": self.get_html_form('book'),
                "select_author": self.get_html_form('author')
            }
        }), status.HTTP_200_OK


class CreateRelView(MethodView):

    model = BookAuthor
    decorators = [login_required(role="admin"), ]

    def dispatch_request(self, *args, **kwargs):
        """
        :book_id - id of book, @int
        :authors_id - list of author ids, @list
        """
        book_id = request.values.get('book_id', None)
        authors_id = request.values.get('authors_id', None)
        if not book_id or not authors_id:
            return json.dumps({
                "status": status.HTTP_400_BAD_REQUEST,
                "reason": "book_id or authors_id is empty."
            }), status.HTTP_400_BAD_REQUEST
        return super(CreateRelView, self).dispatch_request(*args, **kwargs)

    def _check_model_if_exist(self, model_class, id):
        try:
            return db_session.query(model_class).get(id)
        except:
            return "Query with id=%s does not exist." % id

    def post(self):
        book_id = request.values['book_id']
        authors_id = json.loads(request.values['authors_id'])
        errors = []
        book = db_session.query(Book).get(book_id)
        if not book:
            return json.dumps({
                "status": status.HTTP_404_NOT_FOUND,
                "reason": "Book with id=%s does not exist." % book_id
            }), status.HTTP_404_NOT_FOUND
        for author_id in authors_id:
            ba = db_session.query(self.model).filter_by(
                author_id=author_id, book_id=book_id).first()
            author = db_session.query(Author).get(author_id)
            if not author:
                return json.dumps({
                    "status": status.HTTP_404_NOT_FOUND,
                    "reason": "Author with id=%s does not exist." %
                    author_id
                }), status.HTTP_404_NOT_FOUND
            if not ba:
                get_or_create(
                    db_session, self.model, book=book, author=author)
            else:
                msg = 'Relation %s - %s already exists' % (
                    book.name, author.name)
                errors.append(msg)
        if len(errors) > 1:
            return json.dumps({
                "status": status.HTTP_206_PARTIAL_CONTENT,
                "reason": "Relations created, but not with all relations. "
                "Reasons: %s" % ', '.join(errors)
            }), status.HTTP_206_PARTIAL_CONTENT
        if len(errors) == 1:
            return json.dumps({
                "status": status.HTTP_206_PARTIAL_CONTENT,
                "reason": "Relations not created. "
                "Reasons: %s" % ', '.join(errors)
            }), status.HTTP_206_PARTIAL_CONTENT
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "Relations successfully created."
        }), status.HTTP_200_OK


class BookAuthorAPI(MethodView):

    model = BookAuthor
    methods = ['DELETE', 'POST']
    decorators = [login_required(role="admin"), ]

    def dispatch_request(self, *args, **kwargs):
        book_id = request.values.get('book_id', None)
        author_id = request.values.get('author_id', None)
        if not book_id or not author_id:
            return json.dumps({
                "status": status.HTTP_400_BAD_REQUEST,
                "reason": "book_id or author_id field is empty."
            }), status.HTTP_400_BAD_REQUEST
        return super(BookAuthorAPI, self).dispatch_request(*args, **kwargs)

    def delete(self):
        book_id = request.values['book_id']
        author_id = request.values['author_id']
        try:
            query = db_session.query(self.model)
            query.filter_by(author_id=author_id, book_id=book_id).delete()
            db_session.commit()
        except AttributeError:
            return json.dumps({
                "status": status.HTTP_404_NOT_FOUND,
                "reason": "Query doest not exist."
            }), status.HTTP_404_NOT_FOUND
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "Row successfully deleted."
        }), status.HTTP_200_OK

    def post(self):
        book_id = request.values['book_id']
        author_id = request.values['author_id']
        try:
            book = db_session.query(Book).get(book_id)
            author = db_session.query(Author).get(author_id)
            ba = BookAuthor(book=book, author=author)
            db_session.add(ba)
            db_session.commit()
        except AttributeError:
            return json.dumps({
                "status": status.HTTP_404_NOT_FOUND,
                "reason": "Query doest not exist."
            }), status.HTTP_404_NOT_FOUND
        except IntegrityError:
            try:
                return json.dumps({
                    "status": status.HTTP_400_BAD_REQUEST,
                    "reason": "Relation %s - %s already exists." % (
                        book.name, author.name)
                }), status.HTTP_400_BAD_REQUEST
            except:
                return json.dumps({
                    "status": status.HTTP_400_BAD_REQUEST,
                    "reason": "Invalid request. Relation already exists."
                }), status.HTTP_400_BAD_REQUEST
        return json.dumps({
            "status": status.HTTP_200_OK,
            "reason": "Relation %s - %s successfully created." % (
                book.name, author.name)
        }), status.HTTP_200_OK


class SearchView(MethodView):

    def get(self):
        name = request.args.get('name', None)
        if name is None:
            books = db_session.query(Book).order_by('-id').all()
        else:
            books = db_session.query(Book).filter(
                Book.name.like(u'%{0}%'.format(name))).order_by('-id').all()
            authors = db_session.query(Author).filter(
                Author.name.like(u'%{0}%'.format(name))).all()
            for author in authors:
                books.extend([
                    ba.book for ba in author.books
                    if ba.book not in books
                ])
        return json.dumps(books, cls=BookAuthorEncoder)
