import factory


from apps.libriary.models import Book, Author, BookAuthor
from database import db_session


class BookFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Book
        sqlalchemy_session = db_session

    name = factory.Sequence(lambda n: u'Book %d' % n)


class AuthorFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Author
        sqlalchemy_session = db_session

    name = factory.Sequence(lambda n: u'Author %d' % n)


class BookAuthorFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = BookAuthor
        sqlalchemy_session = db_session

    book = factory.SubFactory(BookFactory)
    author = factory.SubFactory(AuthorFactory)
