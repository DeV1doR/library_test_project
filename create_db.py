from apps.libriary.models import User
from apps.libriary.factories import (
    BookAuthorFactory, BookFactory, AuthorFactory)
from database import db_session, drop_db, init_db


def gen_test_data():
    drop_db()
    init_db()
    for x in xrange(10):
        ba = BookAuthorFactory()
        db_session.add(ba)

    book = BookFactory()
    author1 = AuthorFactory()
    author2 = AuthorFactory()
    ba = BookAuthorFactory(book=book, author=author1)
    db_session.add(ba)
    ba = BookAuthorFactory(book=book, author=author2)
    db_session.add(ba)

    book1 = BookFactory()
    book2 = BookFactory()
    author = AuthorFactory()
    ba = BookAuthorFactory(book=book1, author=author)
    db_session.add(ba)
    ba = BookAuthorFactory(book=book2, author=author)
    db_session.add(ba)

    admin = User(username='admin', password='admin')
    admin.role = 'admin'
    db_session.add(admin)

    admin = User(username='foo', password='foo')
    admin.role = 'user'
    db_session.add(admin)
    db_session.commit()


if __name__ == '__main__':
    gen_test_data()
