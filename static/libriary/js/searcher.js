$(function() {
    $("#search_form").find('input').keyup(function(event) {
        var query = $(this).val().trim();
        $.ajax({
            url: CONFIG.search_url + '?name=' + query,
            contentType: "application/json",
            dataType: "json",
            success: function(result) {
                var books = result, table = $('tbody');
                table.children( 'tr:not(:first)' ).remove();
                new_row(books);
                table.highlight(query);
            }
        });
    });

    $('tbody').on('click', '.button-delete', function(e) {
        e.preventDefault();
        var block = $(this).parent(),
            glies_to_show = [
                '.button-delete-confirm', '.button-delete-decline', '.q-delete'
            ],
            glies_to_hide = [
                '.button-edit-confirm', '.button-edit-decline', '.button-delete', '.name'
            ];
        gly_show_hide(glies_to_show, glies_to_hide, block);
        console.log('delete');
    });

    $('tbody').on('click', '.button-delete-confirm', function(e) {
        e.preventDefault();
        var block = $(this).parent(),
            tr = $(this).parents('tr'),
            book_id = tr.find('td[id^="id_"]').attr('id').match(/\d+/)[0],
            author_id = block.attr('id').match(/\d+/)[0],
            api_url = CONFIG.books_authors_url,
            block_result = block.find('span.for-status-result'),
            type = /\_([^_]+)\_/.exec(block.attr('id'))[1];
        $.ajax({
            method: 'DELETE',
            url: api_url,
            data: {book_id: book_id, author_id: author_id},
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                if (type == 'books') {
                    block.parents('tr').remove(); // remove row
                } else {
                    if (block.parent().find('li').length > 1) {
                        block.remove();
                    } else {
                        tr.remove();
                    }
                }
                refresh_select_call('complex');
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
        console.log('delete-confirm');
    });

    $('tbody').on('click', '.button-delete-decline', function(e) {
        e.preventDefault();
        var block = $(this).parent(),
            glies_to_show = [
                '.button-edit-confirm', '.button-edit-decline', '.button-delete', '.name'
            ];
            glies_to_hide = [
                '.button-delete-confirm', '.button-delete-decline', '.q-delete'
            ];
        gly_show_hide(glies_to_show, glies_to_hide, block);
        console.log('delete');
    });

    $('tbody').on('click', '.button-add-new', function(e) {
        e.preventDefault();
        var tr = $(this).parents('tr'),
            select_html = '<select>' +
                            $('select[name="author"]').clone().html() +
                          '</select>',
            accept_gly = "<a class='button-accept-select' href='#'>" +
                            "<span class='glyphicon glyphicon-ok'></span>" +
                         "</a>",
            block_result = "<span class='for-status-result'></span>",
            on_show = function() {
                tr.find('ul').append('<li>' + select_html + accept_gly + block_result + '</li>');
            },
            on_hide = function() {
                tr.find('li').last().remove();
            },
            gly = $(this).find('span.glyphicon');
        plus_minus_gly(gly, on_show, on_hide);
    });

    $('tbody').on('click', '.button-accept-select', function(e) {
        var block = $(this).parent(),
            tr = $(this).parents('tr'),
            book_id = tr.find('td[id^="id_"]').attr('id').match(/\d+/)[0],
            author_id = tr.find(':selected').val(),
            api_url = CONFIG.books_authors_url,
            block_result = block.find('span.for-status-result');
        $.ajax({
            method: 'POST',
            url: api_url,
            data: {book_id: book_id, author_id: author_id},
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                console.log(result);
                $.get(CONFIG.authors_url + author_id).done(function(result) {
                    var response = JSON.parse(result),
                        new_row = '<li id="id_authors_' + author_id + '">' +
                                    '<span class="name">' + response.name + '</span>' +
                                    icons_block + "<span class='for-status-result'></span>" +
                                  '</li>';
                    $(new_row).insertBefore(block);
                    refresh_select_call('complex');
                });
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
        console.log("confirm");
        e.preventDefault();
    });
    $('.button-add-new-type').on('click', function(e) {
        e.preventDefault();
        var self = $(this),
            on_show = function() {
                self.siblings('.form_create').show();
            },
            on_hide = function() {
                self.siblings('.form_create').hide();
            },
            gly = $(this).find('span.glyphicon');
        plus_minus_gly(gly, on_show, on_hide);
    });
    $('.form_create > .btn').on('click', function(e) {
        e.preventDefault();
        var form = $(this).parents('form'),
            type = form.find('input[type="text"]').attr('name'),
            name = form.find('input[type="text"]').val(),
            api_url = form.find('input[type="hidden"]').val(),
            block_result = form.siblings('.for-status-result');
        $.ajax({
            method: 'POST',
            url: api_url,
            data: {name: name},
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                console.log(result);
                if (type == 'book') {
                    var books = [data.data];
                    new_row(books);
                }
                refresh_select_call('complex');
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
        form.siblings('a').click();
        form.find('input[type="text"]').val('');
    });

});