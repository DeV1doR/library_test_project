$(function() {
    $('.button-add-new-relat').on('click', function(e) {
        e.preventDefault();
        var on_show = function() {
            $(".button-create-rel").show('slow');
            $("tbody tr:first").show('slow');
        };
        var on_hide = function() {
            $(".button-create-rel").hide('slow');
            $("tbody tr:first").hide('slow');
        };
        var gly = $(this).find('span.glyphicon');
        plus_minus_gly(gly, on_show, on_hide);
        // if (gly.hasClass('glyphicon-plus')) {
        //     gly.addClass('glyphicon-minus');
        //     gly.removeClass('glyphicon-plus');
        //     on_show();
        // } else {
        //     gly.addClass('glyphicon-plus');
        //     gly.removeClass('glyphicon-minus');
        //     on_hide();
        // }
    });
    $('tbody tr:first').on('click', '.button-accept-select', function(e) {
        e.preventDefault();
        e.stopPropagation(); // for prevent triggering the same event
        $(this).parent().after($(this).parent().clone()); // copy select list to next li
        var gly_remove = "<a class='button-remove-rel-create' href='#'>" +
                            "<span class='glyphicon glyphicon-remove'></span>" +
                         "</a>";
        $(this).parent().append(gly_remove);
        $(this).remove(); // remove this ok-gly
    });
    $('tbody tr:first').on('click', '.button-remove-rel-create', function(e) {
        e.preventDefault();
        $(this).parent().remove(); // remove li
    });
    $('.button-create-rel').on('click', function(e) {
        e.preventDefault();
        var book_id = $('tbody tr:first td:first select').val(),
            authors_id = [],
            url = CONFIG.rel_create_url;
        $.each($('tbody tr:first td:eq(1) select'), function() {
            authors_id.push($(this).val());
        });
        $.ajax({
            method: 'POST',
            url: url,
            data: {book_id: book_id, authors_id: JSON.stringify(authors_id)},
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                if (data.status != 206) {
                    $('.button-add-new-relat').click();
                    refresh_select_call('complex');
                }
                $("#search_form").find('input').val('');
                $("#search_form").find('input').keyup();
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
    });
});