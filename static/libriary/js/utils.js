function new_row(books) {
    /**
    * Update table rows with new data;
    * :books - list of books from json api, @array;
    **/
    var table = $('tbody');
    for (var i = 0; i < books.length; i++) {
        var book = books[i];
        if (book.authors.length <= 0) {
            continue;
        }
        var tr = "<tr>";
        tr +=       "<td id=id_books_'" + book.id + "'>";
        tr +=           "<input type='hidden' value='" + CONFIG.books_url + "' />";
        tr +=           "<span class='name'>" + book.name + "</span>";
        if (current_user.has_permissions) {
            tr +=               icons_block;
        }
        tr +=           "<span class='for-status-result'></span>";
        tr +=       "</td>";
        tr +=       "<td>";
        tr +=           "<ul>";
        for (var j = 0; j < book.authors.length; j++) {
            var author = book.authors[j];
            tr +=           "<li id=id_authors_'" + author.id + "'>";
            tr +=               "<input type='hidden' value='" + CONFIG.authors_url + "' />";
            tr +=               "<span class='name'>" + author.name + "</span>";
            if (current_user.has_permissions) {
                tr +=               icons_block;
            }
            tr +=               "<span class='for-status-result'></span>";
            tr +=           "</li>";
        }
        tr +=           "</ul>";
        tr +=       "</td>";
        if (current_user.has_permissions) {
            tr +=       "<td>";
            tr +=           "<a class='button-plus button-add-new' href='#'>";
            tr +=               "<span class='glyphicon glyphicon-plus'></span>";
            tr +=           "</a>";
            tr +=       "</td>";
        }
        tr +=    "</tr>";
        table.append(tr);
    }
}
function show_response_message(status, msg) {
    /**
    * Unique handler for show result message:
    * :status - status of response, needed for color notif., @int
    * :msg - kind of message, which will be represent on message block, @string
    * TODO: add responsive block without scaling of window;
    **/
    var msg_block = $('.customize-answer'),
        msg_class;
    if (status == 400 || status == 404 || status == 500) {
        msg_class = 'bg-danger';
    } else if (status == 200) {
        msg_class = 'bg-success';
    } else if (status == 206) {
        msg_class = 'bg-warning';
    }
    $.each(['bg-danger', 'bg-success', 'bg-warning'], function(i, value) {
        msg_block.removeClass(value);
    });
    msg_block.find('.show-response-answer').empty();
    msg_block.find('.show-response-answer').html(msg);
    msg_block.addClass(msg_class);
    msg_block.slideToggle("slow");
    setTimeout(function() {
        msg_block.slideToggle("slow");
    }, 5000);
}
function plus_minus_gly(gly, callback_on_show, callback_on_hide) {
    /**
    * Used for plus-minus click with two callbacks:
    * :gly - glyphicon which clicked, @$(object);
    * :callback_on_show - function, in which implement logic after plus was clicked, @void;
    * :callback_on_hide - reverse to callback_on_show, @void;
    **/
    if (gly.hasClass('glyphicon-plus')) {
        gly.addClass('glyphicon-minus');
        gly.removeClass('glyphicon-plus');
        callback_on_show();
    } else {
        gly.addClass('glyphicon-plus');
        gly.removeClass('glyphicon-minus');
        callback_on_hide();
    }
}
function gly_show_hide(glies_to_show, glies_to_hide, p_block) {
    /**
    * Used for show glyphicons or hide it by multiple selection;
    **/
    if (!p_block) {
        f_block = function(el) { return $(el); };
    } else {
        f_block = function(el) { return p_block.find(el); };
    }
    for (var i = 0; i < glies_to_show.length; i++) {
        f_block(glies_to_show[i]).show();
    }

    for (var j = 0; j < glies_to_hide.length; j++) {
        f_block(glies_to_hide[j]).hide();
    }
}
function refresh_type_selects(select_book, select_author) {

    (function refresh_table_select() {
        var r1_c1 = $('tbody tr:first td:first'), r1_c2 = $('tbody tr:first td:eq(1)'),
            link_gly = $('.button-plus.button-add-new:first'),
            link_gly_n_submit = $('.button-add-new-relat');

        // need to click first plus if it minus yet
        if (link_gly.find('.glyphicon').hasClass('glyphicon-minus')) {
            link_gly[0].click();
        }
        if (link_gly_n_submit.find('.glyphicon').hasClass('glyphicon-minus')) {
            link_gly_n_submit[0].click();
        }
        // clear 1 and 2 col
        r1_c1.empty();
        r1_c2.find('ul').empty();

        // fill 1 and 2 col
        r1_c1.append(select_book);
        r1_c2.find('ul').append('<li>' + select_author + '</li>');
    })();
    (function refresh_edit_select() {
        var edit_b = $('.row.row-edit:eq(2) .col-md-12'),
            book_s = edit_b.find('select[name="book"]'),
            author_s = edit_b.find('select[name="author"]'),
            current_type_s = $('.row.row-edit:eq(1) select');
            selected_type = $('.row.row-edit select.selected_type').attr('name');

        // clear first
        book_s.remove();
        author_s.remove();

        // add updated selects
        edit_b.prepend(select_book);
        edit_b.prepend(select_author);

        // check for marked current selection
        if (selected_type == 'book') {
            edit_b.find('select[name="author"]')
                .hide()
                .addClass('hidden-input');
            edit_b.find('select[name="book"]')
                .addClass('hidden-input selected_type')
                .css('display', 'inline-block');
        } else if (selected_type == 'author') {
            edit_b.find('select[name="book"]')
                .hide()
                .addClass('hidden-input');
            edit_b.find('select[name="author"]')
                .addClass('hidden-input selected_type')
                .css('display', 'inline-block');
        } else {
            edit_b.find('select[name="book"]')
                .hide()
                .addClass('hidden-input');
            edit_b.find('select[name="author"]')
                .hide()
                .addClass('hidden-input');
        }
    })();
}
function refresh_select_call(select_type) {
    // select_type - @str
    $.get(CONFIG.main_page_url + '?form=' + select_type)
        .done(function(result) {
            var response = JSON.parse(result);
            // show_response_message(response.status, response.reason);
            refresh_type_selects(
                response.data.select_book, response.data.select_author);
        });
}