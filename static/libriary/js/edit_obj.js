$(function() {
    $('select[name="type"]').change(function(e) {
        var selected = $(this).find(':selected').text(),
            book = $(this).parents('.edit-right').find('select[name="book"]'),
            author = $(this).parents('.edit-right').find('select[name="author"]'),
            mark_class = 'selected_type',
            glies_to_show = [
                '.menu-edit.button-edit', '.menu-edit.button-delete'
            ],
            glies_to_hide = [
                '.menu-edit.button-edit-confirm', '.menu-edit.button-edit-decline'
            ];

        $('.menu-edit.button-edit-decline').click();
        gly_show_hide(glies_to_show, glies_to_hide);

        if (selected == 'Book') {
            book.show();
            book.addClass(mark_class);
            author.hide();
            author.removeClass(mark_class);
            
        } else if (selected == 'Author') {
            author.show();
            author.addClass(mark_class);
            book.hide();
            book.removeClass(mark_class);
        }
    });
    $('.menu-edit.button-edit').on('click', function(e) {
        e.preventDefault();
        var type_text = $('.selected_type').find(':selected').text(),
            type_name = $('.selected_type').attr('name'),
            type_id = $('.selected_type').val(),
            input = $('input[name="edit_name"]'),
            glies_to_show = [
                '.menu-edit.button-edit-confirm', '.menu-edit.button-edit-decline'
            ],
            glies_to_hide = [
                '.menu-edit.button-delete', '.menu-edit.button-edit', '.selected_type'
            ];
        input.show();
        input.val(type_text);
        input.attr('id', 'id_' + type_name + '_' + type_id);
        gly_show_hide(glies_to_show, glies_to_hide);
    });

    $('.menu-edit.button-edit-confirm').on('click', function(e) {
        e.preventDefault();
        var api_url,
            input = $(this).siblings('input[type="text"]'),
            type_text = input.val(),
            type_name = $('.selected_type').attr('name'),
            type_id = input.attr('id').match(/\d+/)[0],
            block_result = $('.menu-edit.for-status-result');
        if (CONFIG.books_url.indexOf(type_name) > -1) {
            api_url = CONFIG.books_url;
        } else if (CONFIG.authors_url.indexOf(type_name) > -1) {
            api_url = CONFIG.authors_url;
        } else {
            console.log('Url not specified.');
        }
        $.ajax({
            url: api_url + type_id,
            method: 'PUT',
            data: {name: type_text},
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                console.log(result);
                $.get(api_url + type_id).done(function(result) {
                    var response = JSON.parse(result);
                    $('.selected_type').find(':selected').html(response.name);
                });
                $('.menu-edit.button-edit-decline').click();
                $("#search_form").find('input').val('');
                $("#search_form").find('input').keyup();
                refresh_select_call('complex');
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
    });

    $('.menu-edit.button-edit-decline').on('click', function(e) {
        e.preventDefault();
        var glies_to_show = [
                '.menu-edit.button-delete', '.menu-edit.button-edit', '.selected_type'
            ],
            glies_to_hide = [
                '.menu-edit.button-edit-confirm', '.menu-edit.button-edit-decline',
                'input[name="edit_name"]'
            ];
        gly_show_hide(glies_to_show, glies_to_hide);
    });

    $('.menu-edit.button-delete').on('click', function(e) {
        e.preventDefault();
        var block = $(this).parent(),
            glies_to_show = [
                '.menu-edit.button-delete-confirm', '.menu-edit.button-delete-decline',
                '.menu-edit.q-delete'
            ],
            glies_to_hide = [
                '.menu-edit.button-edit', '.menu-edit.button-delete'
            ];

        gly_show_hide(glies_to_show, glies_to_hide);
        console.log('delete');
    });

    $('.menu-edit.button-delete-confirm').on('click', function(e) {
        e.preventDefault();
        var api_url,
            input = $(this).siblings('input[type="text"]'),
            type_name = $('.selected_type').attr('name'),
            type_id = $('.selected_type').val(),
            block_result = $('.menu-edit.for-status-result');
        if (CONFIG.books_url.indexOf(type_name) > -1) {
            api_url = CONFIG.books_url;
        } else if (CONFIG.authors_url.indexOf(type_name) > -1) {
            api_url = CONFIG.authors_url;
        } else {
            console.log('Url not specified.');
        }
        $.ajax({
            method: 'DELETE',
            url: api_url + type_id,
            success: function(result) {
                var data = JSON.parse(result);
                show_response_message(data.status, data.reason);
                console.log(result);
                // $('.selected_type').find(':selected').remove();
                $('.menu-edit.button-delete-decline').click();
                $("#search_form").find('input').val('');
                $("#search_form").find('input').keyup();
                refresh_select_call('complex');
            },
            error: function(result) {
                var data = JSON.parse(result.responseText);
                show_response_message(data.status, data.reason);
                console.log(result);
            }
        });
        console.log('delete-confirm');
    });

    $('.menu-edit.button-delete-decline').on('click', function(e) {
        e.preventDefault();
        var block = $(this).parent(),
            glies_to_show = [
                '.menu-edit.button-edit', '.menu-edit.button-delete'
            ],
            glies_to_hide = [
                '.menu-edit.button-delete-confirm', '.menu-edit.button-delete-decline',
                '.menu-edit.q-delete'
            ];

        gly_show_hide(glies_to_show, glies_to_hide);
        console.log('delete-decline');
    });
});